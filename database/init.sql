CREATE DATABASE auction;

CREATE TABLE bidders (
  bidderid integer primary key,
  lastname varchar(100),
  firstname varchar(100),
  address varchar(200),
  phone varchar(14)
);

CREATE TABLE items (
  itemid int primary key,
  name varchar(100),
  description text,
  resaleprice decimal(10,2),
  winbidder int,
  winprice decimal(10,2)
);

CREATE TABLE admins (
  userid varchar(20) primary key,
  name varchar(100),
  password char(64)
);