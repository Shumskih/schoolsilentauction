<?php
session_start();
include("model/bidder.php");
include("model/item.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>AuctionHelper</title>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <script language="javascript" type="text/javascript">
        function getRealTime() {
            // retrieve the DOM objects to place the content
            let dombidders = document.getElementById("biddercount");
            let domitems = document.getElementById("itemcount");
            let domitemtotal = document.getElementById("itemtotal");
            let dombidtotal = document.getElementById("bidtotal");
            //send the GET request to retrieve the data
            let request = new XMLHttpRequest();
            request.open("GET", "realtime.php", true);
            request.onreadystatechange = function() {
                if (request.readyState === 4 && request.status === 200) {
                    //parse the XML document to get each data element
                    let xmldoc = request.responseXML;

                    let xmlbidders = xmldoc.getElementsByTagName("bidders")[0];
                    let bidders = xmlbidders.childNodes[0].nodeValue;

                    let xmlitems = xmldoc.getElementsByTagName("items")[0];
                    let items = xmlitems.childNodes[0].nodeValue;

                    let xmlitemtotal = xmldoc.getElementsByTagName("itemtotal")[0];
                    let itemtotal = xmlitemtotal.childNodes[0].nodeValue;

                    let xmlbidtotal = xmldoc.getElementsByTagName("bidtotal")[0];
                    let bidtotal = xmlbidtotal.childNodes[0].nodeValue;

                    dombidders.innerHTML = bidders;
                    domitems.innerHTML = items;
                    domitemtotal.innerHTML = itemtotal;
                    dombidtotal.innerHTML = bidtotal;
                }
            };
            request.send();
        }
    </script>
</head>
<body>
<header>
    <?php include("header.inc.php"); ?>
</header>
<section id="container">
    <nav>
        <?php include ("nav.inc.php"); ?>
    </nav>
    <main>
        <?php
        if (isset($_REQUEST['content']) && strpos($_REQUEST['content'], "bidder")) {
            include("parts/bidder/" . $_REQUEST['content'] . ".inc.php");
        } elseif (isset($_REQUEST['content']) && strpos($_REQUEST['content'], "item")) {
            include("parts/item/" . $_REQUEST['content'] . ".inc.php");
        } else {
            include("main.inc.php");
        }
        ?>
    </main>
    <aside>
        <?php include("aside.inc.php") ?>
        <script language="javascript" type="text/javascript")>
            getRealTime();
            setInterval(getRealTime, 5000);
        </script>
    </aside>
</section>
<footer>
    <?php include("footer.inc.php") ?>
</footer>
</body>
</html>

